import React from "react";
import "./footer.scss";

const Footer = () => (
    <footer className="footer">
        <p className="footer__content">Album Player App</p>
    </footer>
);

export default Footer;
