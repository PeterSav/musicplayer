import React from "react";
import "./albumsLink.scss";
import { Link } from "react-router-dom";

const AlbumsLink = ({ cover, id, title }) => (
    <div className="albumsLink__album">
        <Link to={`/albums/${id}`} className="albumsLink__album__link">
            <figure>
                <img
                    src={cover}
                    alt="album cover"
                    className="albumsLink__album__img"
                />
            </figure>
            <figcaption>{title}</figcaption>
        </Link>
    </div>
);

export default AlbumsLink;
