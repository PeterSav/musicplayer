import React, { useEffect } from "react";
import "./searchBar.scss";
import { connect } from "react-redux";
import { handleSearch, searchInitialize } from "../../../../models/actions";

const SearchBar = ({ search, handleSearch, searchInitialize }) => {
    useEffect(() => {
        searchInitialize();
    }, []);

    return (
        <>
            <h2 className="searchBar__title">Search to find an album</h2>
            <p className="searchBar__subtitle">
                Lorem ipsum, dolor sit amet consectetur adipisicing elit.
            </p>
            <input
                type="text"
                placeholder="Search..."
                name="search"
                value={search}
                onChange={e => handleSearch(e.target.value)}
                className="searchBar__search"
            />
        </>
    );
};

const mapStateToProps = state => ({
    search: state.search,
});

const mapDispatchToProps = dispatch => ({
    handleSearch: payload => dispatch(handleSearch(payload)),
    searchInitialize: () => dispatch(searchInitialize()),
});

export default connect(mapStateToProps, mapDispatchToProps)(SearchBar);
