import React from "react";
import "./albumsList.scss";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import SearchBar from "./searchBar/index";
import AlbumsLink from "./albumsLink/index";

const AlbumsList = ({ albumsData, hasSearched, sortedAlbums }) => {
    const showAlbums = () => {
        const albumsToShow = hasSearched ? sortedAlbums : albumsData;

        return albumsToShow.map(({ cover, title }, id) => (
            <AlbumsLink cover={cover} key={id} id={id} title={title} />
        ));
    };

    return (
        <>
            <SearchBar />
            <div className="albumsList">{showAlbums()}</div>
        </>
    );
};

const mapStateToProps = state => ({
    albumsData: state.albumsData,
    hasSearched: !!state.search,
    sortedAlbums: state.sortedAlbums,
});

export default connect(mapStateToProps)(AlbumsList);
