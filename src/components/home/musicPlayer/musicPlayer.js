import React from "react";
import "./musicPlayer.scss";
import { connect } from "react-redux";
import previous from "../../../assets/leftChevron.svg";
import next from "../../../assets/rightChevron.svg";
import { goToTrack, goToNextTrack } from "../../../models/actions";

const MusicPlayer = ({
    albumsData,
    goToTrack,
    hasPickedSong,
    indexOfAlbum,
    indexOfTrack,
    goToNextTrack,
}) => {
    const showGoToNext =
        indexOfTrack + 1 < albumsData[indexOfAlbum].tracks.length;
    const showGoToPrevious = indexOfTrack > 0;
    const index = indexOfTrack;

    return (
        <div className="musicPlayer">
            <div className="musicPlayer__container">
                <h5 className="musicPlayer__container__songTitle">
                    {hasPickedSong
                        ? albumsData[indexOfAlbum].tracks[indexOfTrack].title
                        : "Please Select A Song"}
                </h5>
                <img
                    onClick={() => goToTrack(index - 1)}
                    src={previous}
                    alt="previous"
                    className={
                        showGoToPrevious
                            ? "musicPlayer__container__controls"
                            : "musicPlayer__container__controls-hidden"
                    }
                />
                <div>
                    <audio
                        onEnded={() => goToNextTrack(index + 1)}
                        src={
                            hasPickedSong
                                ? albumsData[indexOfAlbum].tracks[indexOfTrack]
                                      .url
                                : undefined
                        }
                        controls
                    ></audio>
                </div>
                <img
                    onClick={() => goToTrack(index + 1)}
                    src={next}
                    alt="next"
                    className={
                        showGoToNext
                            ? "musicPlayer__container__controls"
                            : "musicPlayer__container__controls-hidden"
                    }
                />
            </div>
        </div>
    );
};

const mapStateToProps = state => ({
    albumsData: state.albumsData,
    hasPickedSong: state.hasPickedSong,
    indexOfAlbum: state.indexOfAlbum,
    indexOfTrack: state.indexOfTrack,
});

const mapDispatchToProps = dispatch => ({
    goToTrack: payload => dispatch(goToTrack(payload)),
    goToNextTrack: payload => dispatch(goToNextTrack(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(MusicPlayer);
