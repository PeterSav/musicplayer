import React from "react";
import "./album.scss";
import { connect } from "react-redux";
import Track from "./track/index";

const Album = ({ match, albumsData }) => {
    const { id } = match.params;
    const album = albumsData[id];

    const showTracks = () =>
        album.tracks.map(({ title, artist, trackCover, url }, index) => (
            <Track
                key={index}
                indexOfAlbum={id}
                indexOfTrack={index}
                title={title}
                artist={artist}
                trackCover={trackCover}
                url={url}
            />
        ));

    return (
        <>
            <h5 className="album__title">{album.title}</h5>
            <div className="album">{showTracks()}</div>
        </>
    );
};

const mapStateToProps = state => ({
    albumsData: state.albumsData,
});

export default connect(mapStateToProps)(Album);
