import React from "react";
import "./track.scss";
import { connect } from "react-redux";
import { selectTrack } from "../../../../models/actions";

//? { title, artist, trackCover, url }, id)

const Track = ({
    title,
    artist,
    trackCover,
    indexOfAlbum,
    indexOfTrack,
    selectTrack,
}) => {
    return (
        <div
            className="track"
            onClick={() => selectTrack(indexOfAlbum, indexOfTrack)}
        >
            <figure>
                <img
                    src={trackCover}
                    alt="album cover"
                    className="track__img"
                />
            </figure>
            <h5>Artist: {artist}</h5>
            <figcaption>
                <b>Title: {title}</b>
            </figcaption>
        </div>
    );
};

const mapDispatchToProps = dispatch => ({
    selectTrack: (indexOfAlbum, indexOfTrack) =>
        dispatch(selectTrack(indexOfAlbum, indexOfTrack)),
});

export default connect(null, mapDispatchToProps)(Track);
