import React from "react";
import "./introToPlayer.scss";

const IntroToPlayer = () => (
    <>
        <h1 className="IntroToPlayer__heading">
            WELCOME TO OUR ALBUM PLAYER APP
        </h1>
        <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maiores
            aliquam quisquam, ut, cumque atque ex quia quis unde odio numquam
            voluptates incidunt voluptatum autem nostrum dignissimos, vitae
            dolorem consequuntur. Architecto. Quia reprehenderit excepturi
            doloremque magnam illo culpa repudiandae autem. Illum saepe quos
            enim doloremque possimus eligendi hic voluptas omnis culpa?
            Veritatis nulla et voluptates sint! Impedit consectetur quasi dolore
            ratione. Ipsum nihil corrupti ex, beatae quia quaerat in quas
            numquam neque provident accusamus voluptate suscipit, officiis
            doloremque dolores mollitia laborum maxime nisi possimus? Culpa
            laborum assumenda atque, sunt ipsa illum. Atque nam id excepturi
            reiciendis facere earum aperiam, amet, voluptates, delectus debitis
            ab quaerat sed nihil! Architecto, excepturi. Consectetur illum esse
            veniam iste dolorem? Architecto illo voluptatem corporis aut
            deserunt. Inventore accusantium voluptatem eos numquam eligendi,
            omnis repellat debitis repellendus in nam modi enim est, magni et
            obcaecati maxime veritatis. Non autem, natus ut iusto unde delectus
            quae eius voluptatibus? Voluptatem numquam maiores culpa iusto
            minima, cumque amet. Omnis eos atque asperiores, nostrum id vel
            harum sit aut molestiae ipsum iure tenetur autem dolores officia ea
            perferendis tempore! Velit, eaque. Laboriosam magnam est odit
            suscipit, expedita quam, tempore iure obcaecati voluptate quaerat
            perspiciatis sit? Facilis id natus sint in esse eius dignissimos
            voluptates est ipsa temporibus, quia adipisci doloremque provident.
            Ratione consequuntur quidem nulla veniam suscipit recusandae cumque
            voluptas laborum explicabo laboriosam, pariatur, quo at aliquam. Sit
            fuga natus dignissimos impedit, earum facere voluptatem quas? Ut sit
            repellendus numquam similique.
        </p>
        <p>
            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Eius
            cupiditate, assumenda at asperiores quidem amet, dignissimos aperiam
            tenetur error ipsam veniam saepe, doloribus reprehenderit ut placeat
            maiores hic atque accusantium. Vitae, a voluptate hic ad, adipisci
            illo aperiam aspernatur corporis eveniet ducimus totam, facere earum
            voluptatibus? Sit sapiente recusandae corrupti sunt ipsa corporis.
            Omnis esse dicta vitae minima iusto! Accusamus. Tempore, delectus
            dolore. Laudantium enim ut sunt, excepturi possimus et rem ipsam
            quod temporibus? Ullam laboriosam possimus praesentium totam eum
            doloribus! Amet vitae praesentium rem dignissimos sequi quibusdam
            eligendi inventore? Assumenda omnis consequatur optio commodi rem
            dolores, hic officiis ipsum doloribus dolorem aliquam nemo tenetur
            modi illo voluptatibus? Harum eveniet veritatis nemo, quisquam nobis
            natus id at. Distinctio, deleniti hic. Porro reprehenderit
            recusandae, expedita voluptatem iste nisi cum molestias quo sunt
            earum vitae veritatis nostrum facere obcaecati culpa minima dolor a
            facilis possimus laudantium natus nesciunt! Iure distinctio itaque
            at. Qui exercitationem vel numquam consectetur quo debitis, eaque
            esse eveniet unde consequatur dolorem assumenda fuga vero sequi
            totam reiciendis nobis fugiat ea id excepturi placeat. Et modi rem
            provident porro. Officiis qui aperiam debitis voluptate dicta nemo
            sint esse nostrum? Eum culpa cum doloribus incidunt officiis.
            Dolorem minus aspernatur quia. Ad nulla hic quos alias reiciendis
            adipisci sint ullam fugiat! Reiciendis sit doloremque tempora in
            accusantium accusamus saepe, repellendus consectetur odio fuga
            veritatis enim, laboriosam, porro ratione minima quae praesentium
            nulla? Consectetur ipsa libero dolore repudiandae atque animi modi
            culpa! Doloribus, autem deserunt sit quidem dicta culpa recusandae
            explicabo pariatur tempore consequatur inventore molestias
            blanditiis obcaecati. Sint odio minima corporis illum exercitationem
            veritatis mollitia quo provident architecto, repellendus rem
            placeat! Ipsum commodi fugiat et aliquam. Quo praesentium magni
            eveniet corporis aliquid et modi deserunt nihil, nostrum ab vel
            tempore recusandae ut repellat itaque iste corrupti at quisquam
            perferendis molestiae reiciendis?
        </p>
    </>
);

export default IntroToPlayer;
