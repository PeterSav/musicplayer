import React from "react";
import "./home.scss";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import IntroToPlayer from "./introToPlayer/index";
import AlbumsList from "./albumsList/index";
import Album from "./album/index";
import MusicPlayer from "./musicPlayer/index";

const Home = () => {
    return (
        <Router>
            <div>
                <header className="header">
                    <Link to="/" className="header__logo">
                        logo
                    </Link>
                    <nav className="header__navigation">
                        <Link
                            to="/albums"
                            className="header__navigation__links"
                        >
                            Albums
                        </Link>
                        <Link to="/" className="header__navigation__links">
                            About us
                        </Link>
                    </nav>
                </header>
                <Route exact path="/albums/" component={AlbumsList} />
                <Route exact path="/" component={IntroToPlayer} />
                <Route exact path={`/albums/:id`} component={Album} />
                <MusicPlayer />
            </div>
        </Router>
    );
};

export default Home;
