import React from "react";
import "./App.scss";
import { createEpicMiddleware } from "redux-observable";
import { Provider } from "react-redux";
import rootReducer from "./models/reducers";
import { rootEpic } from "./models/epics";
import { createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import Footer from "./components/footer/index";
import Home from "./components/home/index";

const epicMiddleware = createEpicMiddleware();

const store = createStore(
    rootReducer,
    composeWithDevTools(applyMiddleware(epicMiddleware))
);

epicMiddleware.run(rootEpic);

const App = () => {
    return (
        <Provider store={store}>
            <div className="wrapper">
                <Home />
            </div>
            <Footer />
        </Provider>
    );
};

export default App;
