import {
    HANDLE_SEARCH,
    HANDLE_SEARCH_SUCCESS,
    SEARCH_INITIALIZE,
    SELECT_TRACK_TO_PLAY,
    SELECT_TRACK_TO_PLAY_SUCCESS,
    GO_TO_TRACK,
    GO_TO_TRACK_SUCCESS,
    GO_TO_NEXT_TRACK_FAIL,
    GO_TO_NEXT_TRACK,
} from "./actions";
import { map } from "rxjs/operators";
import { ofType, combineEpics } from "redux-observable";

const handleSearchEpic = (action$, state$) =>
    action$.pipe(
        ofType(HANDLE_SEARCH),
        map(action => {
            const { payload } = action;
            const newSortedAlbums = [];
            state$.value.albumsData.forEach(album => {
                if (album.title.toLowerCase().includes(payload.toLowerCase())) {
                    newSortedAlbums.push(album);
                }
            });
            // ?check if someone wrote or if he deleted all the search
            if (payload) {
                return {
                    type: HANDLE_SEARCH_SUCCESS,
                    payload: { search: payload, newSortedAlbums },
                };
            } else {
                return {
                    type: SEARCH_INITIALIZE,
                };
            }
        })
    );

const selectTrackEpic = (action$, state$) =>
    action$.pipe(
        ofType(SELECT_TRACK_TO_PLAY),
        map(action => {
            const { indexOfAlbum, indexOfTrack } = action.payload;
            const albumSelected = state$.value.albumsData[indexOfAlbum];
            const currentSongId = indexOfTrack;
            const showGoToNext = indexOfTrack + 1 < albumSelected.tracks.length;
            const showGoToPrevious = indexOfTrack > 0;
            return {
                type: SELECT_TRACK_TO_PLAY_SUCCESS,
                payload: {
                    albumSelected,
                    currentSongId,
                    showGoToNext,
                    showGoToPrevious,
                    hasPickedSong: true,
                },
                indexOfAlbum,
                indexOfTrack,
            };
        })
    );

const goToTrack = action$ =>
    action$.pipe(
        ofType(GO_TO_TRACK),
        map(action => {
            const { payload } = action;
            return { type: GO_TO_TRACK_SUCCESS, payload };
        })
    );

const goToNextTrackEpic = (action$, state$) =>
    action$.pipe(
        ofType(GO_TO_NEXT_TRACK),
        map(action => {
            const { payload } = action;
            const albumId = state$.value.indexOfAlbum;
            const canGoNext =
                payload < state$.value.albumsData[albumId].tracks.length;
            if (canGoNext) {
                return {
                    type: GO_TO_TRACK_SUCCESS,
                    payload,
                };
            } else {
                return { type: GO_TO_NEXT_TRACK_FAIL };
            }
        })
    );

export const rootEpic = combineEpics(
    handleSearchEpic,
    selectTrackEpic,
    goToTrack,
    goToNextTrackEpic
);
