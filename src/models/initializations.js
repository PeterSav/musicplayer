import cover from "../assets/nightwishAlbum.jpg";
import trackCover from "../assets/trackCover.jpg";

const url = `https://docs.google.com/uc?export=download&id=1rBxfB345zC0LlhfnFvHqeDfWH2SM6nmf`;

const albumsData = [
    {
        title: "album 1",
        cover,
        tracks: [
            {
                title: "track 1",
                artist: "NIGHTWISH",
                trackCover,
                url,
            },
            {
                title: "track 2",
                artist: "NIGHTWISH",
                trackCover,
                url,
            },
            {
                title: "track 3",
                artist: "NIGHTWISH",
                trackCover,
                url,
            },
            {
                title: "track 4",
                artist: "NIGHTWISH",
                trackCover,
                url,
            },
            {
                title: "track 5",
                artist: "NIGHTWISH",
                trackCover,
                url,
            },
        ],
    },
    {
        title: "album 2",
        cover,
        tracks: [
            {
                title: "track 1",
                artist: "NIGHTWISHERINO",
                trackCover,
                url,
            },
            {
                title: "track 2",
                artist: "NIGHTWISHERINO",
                trackCover,
                url,
            },
            {
                title: "track 3",
                artist: "NIGHTWISHERINO",
                trackCover,
                url,
            },
            {
                title: "track 4",
                artist: "NIGHTWISHERINO",
                trackCover,
                url,
            },
            {
                title: "track 5",
                artist: "NIGHTWISHERINO",
                trackCover,
                url,
            },
        ],
    },
    {
        title: "album 3",
        cover,
        tracks: [
            {
                title: "track 1",
                artist: "NightWishshshshsh",
                trackCover,
                url,
            },
            {
                title: "track 2",
                artist: "NightWishshshshsh",
                trackCover,
                url,
            },
            {
                title: "track 3",
                artist: "NightWishshshshsh",
                trackCover,
                url,
            },
            {
                title: "track 4",
                artist: "NightWishshshshsh",
                trackCover,
                url,
            },
            {
                title: "track 5",
                artist: "NightWishshshshsh",
                trackCover,
                url,
            },
        ],
    },
];

export default albumsData;
