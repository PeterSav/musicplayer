export const HANDLE_SEARCH = "HANDLE_SEARCH";
export const HANDLE_SEARCH_SUCCESS = "HANDLE_SEARCH_SUCCESS";
export const SEARCH_INITIALIZE = "SEARCH_INITIALIZE";
export const SELECT_TRACK_TO_PLAY = "SELECT_TRACK_TO_PLAY";
export const SELECT_TRACK_TO_PLAY_SUCCESS = "SELECT_TRACK_TO_PLAY_SUCCESS";
export const GO_TO_TRACK = "GO_TO_NEXT_TRACK";
export const GO_TO_TRACK_SUCCESS = "GO_TO_NEXT_TRACK_SUCCESS";
export const GO_TO_NEXT_TRACK = "GO_TO_NEXT_TRACK";
export const GO_TO_NEXT_TRACK_FAIL = "GO_TO_NEXT_TRACK_FAIL";

export const handleSearch = payload => ({
    type: HANDLE_SEARCH,
    payload,
});

export const searchInitialize = () => ({
    type: SEARCH_INITIALIZE,
});

export const selectTrack = (indexOfAlbum, indexOfTrack) => ({
    type: SELECT_TRACK_TO_PLAY,
    payload: { indexOfAlbum, indexOfTrack },
});

export const goToTrack = payload => ({
    type: GO_TO_TRACK,
    payload,
});

export const goToNextTrack = payload => ({
    type: GO_TO_NEXT_TRACK,
    payload,
});
