import {
    HANDLE_SEARCH_SUCCESS,
    SEARCH_INITIALIZE,
    SELECT_TRACK_TO_PLAY_SUCCESS,
    GO_TO_TRACK_SUCCESS,
} from "./actions";

import albumsData from "./initializations";

const initialState = {
    search: "",
    albumsData,
    sortedAlbums: [],
    hasPickedSong: false,
    indexOfAlbum: 0,
    indexOfTrack: 0,
};

const rootReducer = (state = initialState, action) => {
    switch (action.type) {
        case HANDLE_SEARCH_SUCCESS:
            return {
                ...state,
                search: action.payload.search,
                sortedAlbums: action.payload.newSortedAlbums,
            };
        case SEARCH_INITIALIZE:
            return {
                ...state,
                search: "",
                sortedAlbums: [],
            };
        case SELECT_TRACK_TO_PLAY_SUCCESS:
            return {
                ...state,
                hasPickedSong: true,
                indexOfAlbum: action.indexOfAlbum,
                indexOfTrack: action.indexOfTrack,
            };
        case GO_TO_TRACK_SUCCESS:
            return {
                ...state,
                indexOfTrack: action.payload,
            };
        default:
            return state;
    }
};

export default rootReducer;
